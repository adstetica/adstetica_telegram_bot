import os
import heapq
import torch
from torch.autograd import Variable
from torchvision import transforms
from PIL import Image

path = os.getcwd()
device = torch.device("cpu")     # device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
models_path = os.path.join(path, 'models')

def load_models():
    models = []
    for model_name in os.listdir(models_path):
        model_path = os.path.join(models_path, model_name)
        model = torch.load(model_path, map_location=device)
        models.append(model)
    return models

models = load_models()

test_transforms = transforms.Compose([transforms.Resize(size=(224,224), interpolation=3),
                                      transforms.ToTensor(),
                                      transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
                                      ])

id_to_class = {0: 'Гипертимный', 1: 'Истероидный', 2: 'Паранояльный', 3: 'Тревожный',
             4: 'Шизоидный', 5: 'Эмотивный', 6: 'Эпилептоидный'}


def predict_image(image):
    global models
    image_tensor = test_transforms(image).float()
    image_tensor = image_tensor.unsqueeze_(0)
    input = Variable(image_tensor)
    input = input.to(device)
    results = []
    for i, model in enumerate(models):
        output = model(input)
        output = output.data.cpu().numpy()
        output_indexes = heapq.nlargest(3, range(len(output[0])), output[0].take)
        first, second = output_indexes[0], output_indexes[1]
        results.append('Model {0}: ({1}, {2})'.format(str(i), id_to_class[first], id_to_class[second]))
    return results