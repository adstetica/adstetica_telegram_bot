#! /usr/bin/env python
# -*- coding: utf-8 -*-
'''
    This script forwards your psychotypes radicals according to photo
'''
# pylint: disable=unused-argument,broad-except

from telegram.ext import Updater, MessageHandler, Filters
from utils import *

TOKEN = '926785754:AAGHakdtVQbcp5FTTcI7_jk-v_Z7yCBIxQU'

# Connecting through my Proxy server on Hetzner 159.69.37.26
REQUEST_KWARGS={
    'proxy_url': 'socks5://maxinstellar:english2011@159.69.37.26:9388/',
}


def get_text(update, context):
    context.bot.send_message(chat_id=update.message.chat_id, text='Привет!') # update.message.text

def get_photo(update, context):
    update.message.photo[-1].get_file().download(custom_path="image.jpg")
    filename = os.path.join(os.getcwd(), 'image.jpg')
    image = Image.open(filename)
    results = predict_image(image)
    for result in results:
        context.bot.send_message(chat_id=update.message.chat_id, text=result) # update.message.text


def main():
    updater = Updater(token=TOKEN, use_context=True, request_kwargs=REQUEST_KWARGS)
    dispatcher = updater.dispatcher
    text_handler = MessageHandler(Filters.text, get_text)
    dispatcher.add_handler(text_handler)
    photo_handler = MessageHandler(Filters.photo, get_photo)
    dispatcher.add_handler(photo_handler)
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()